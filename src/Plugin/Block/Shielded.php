<?php

namespace Drupal\nz_shielded\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "nz_shielded",
 *   admin_label = @Translation("Shielded"),
 *   category = @Translation("Shielded")
 * )
 */
class Shielded extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => 'button',
    ] + parent::defaultConfiguration();
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $theme = NULL;
    $size = NULL;
    switch ($this->configuration['type']) {
      case 'big':
      case 'small':
        $theme = 'tab';
        $size = $this->configuration['type'];
        break;

      default:
        $theme = $this->configuration['type'];
        break;
    }

    return [
      '#theme' => $theme,
      '#size' => $size,
      '#cache' => [
        'contexts' => [
          'theme',
        ],
      ],
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select an option'),
      '#options' => [
        'button' => $this->t('Button'),
        'big' => $this->t('Large tab'),
        'small_tab' => $this->t('Small tab'),
      ],
      '#default_value' => $this->configuration['type'],
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['type'] = $form_state->getValue('type');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return Cache::mergeTags(parent::getCacheTags(), ['nz_shielded']);
  }

}
